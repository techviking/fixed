package fixed

// release under the terms of file license.txt

// Only 18 significant digits are supported due to NaN

import (
	"fmt"
	"math"
)

// Fixed is a fixed precision 38.24 number (supports 11.7 digits). It supports NaN.
type Fixed struct {
	main     int64
	mantissa int64
	fp       int64
	sign     int64
	exponent int64
	nan      *ErrNaN
}

var (
	NaN  = &Fixed{
		nan: &ErrNaN{
			reason: errNaNDefault,
		},
	}
	ZERO = &Fixed{fp: 0}
)

// Sign returns:
//
// 	-1 if f <  0
// 	 0 if f == 0 or NaN
// 	+1 if f >  0
//
func (f Fixed) Sign() int64 {
	return f.sign
}

// Add adds f0 to f producing a Fixed. If either operand is NaN, NaN is returned
func (f Fixed) Add(f0 Fixed) *Fixed {
	if f.IsNaN() {
		return FromNaN(f.nan)
	}
	if f0.IsNaN() {
		return FromNaN(f.nan)
	}

	return FromInt64(f.fp+f0.fp, f.exponent)

}

// Sub subtracts f0 from f producing a Fixed. If either operand is NaN, NaN is returned
func (f Fixed) Sub(f0 Fixed) *Fixed {
	if f.IsNaN() {
		return FromNaN(f.nan)
	}
	if f0.IsNaN() {
		return FromNaN(f.nan)
	}

	return FromInt64(f.fp-f0.fp, f.exponent)
}

// Abs returns the absolute value of f. If f is NaN, NaN is returned
func (f Fixed) Abs() *Fixed {
	if f.IsNaN() {
		return &f
	}

	return &Fixed{
		fp:       f.fp,
		main:     f.main,
		mantissa: f.mantissa,
		sign:     1,
		exponent: f.exponent,
	}
}

// Mul multiplies f by f0 returning a Fixed. If either operand is NaN, NaN is returned
func (f Fixed) Mul(f0 Fixed) *Fixed {
	if f.IsNaN() {
		return FromNaN(f.nan)
	}
	if f0.IsNaN() {
		return FromNaN(f.nan)
	}

	return FromInt64(
		f.fp*f0.fp,
		f.exponent,
	)
}

// Div divides f by f0 returning a Fixed. If either operand is NaN, NaN is returned
func (f Fixed) Div(f0 Fixed) *Fixed {
	if f.IsNaN() {
		return FromNaN(f.nan)
	}
	if f0.IsNaN() {
		return FromNaN(f.nan)
	}

	return FromFloat(f.Float() / f0.Float())
}

// Round returns a rounded Fixed(half-up, away from zero) to n decimal places
func (f Fixed) Round(n int) *Fixed {
	if f.IsNaN() {
		return &f
	}

	frac := float64(f.mantissa) * math.Pow10(int(f.exponent*-1)+n)

	return Parse(
		fmt.Sprintf(
			"%d.%d",
			f.main,
			int64(math.Round(frac)),
		),
	)
}

// Equal returns true if the f == f0. If either operand is NaN, false is returned. Use IsNaN() to test for NaN
func (f Fixed) Equal(f0 *Fixed) bool {
	if f.IsNaN() || f0.IsNaN() {
		return false
	}
	return f.Cmp(f0) == 0
}

// GreaterThan tests Cmp() for 1
func (f Fixed) GreaterThan(f0 *Fixed) bool {
	return f.Cmp(f0) == 1
}

// GreaterThaOrEqual tests Cmp() for 1 or 0
func (f Fixed) GreaterThanOrEqual(f0 *Fixed) bool {
	cmp := f.Cmp(f0)
	return cmp == 1 || cmp == 0
}

// LessThan tests Cmp() for -1
func (f Fixed) LessThan(f0 *Fixed) bool {
	return f.Cmp(f0) == -1
}

// LessThan tests Cmp() for -1 or 0
func (f Fixed) LessThanOrEqual(f0 *Fixed) bool {
	cmp := f.Cmp(f0)
	return cmp == -1 || cmp == 0
}

// Cmp compares two Fixed.  Returns:
//
// if f == f0 : 0
// if f > f0 : 0
// if f < f0 : -1
// if both are NaN :  0.
// if f is NaN : 1
// if f0 is NaN, return -1
func (f Fixed) Cmp(f0 *Fixed) int {
	switch {
	case f.IsNaN() && f0.IsNaN():
		return 0
	case f.IsNaN():
		return 1
	case f0.IsNaN():
		return -1
	case f.fp == f0.fp:
		return 0
	case f.fp < f0.fp:
		return -1
	default:
		return 1
	}
}
