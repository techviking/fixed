package fixed

import (
	"fmt"
	"math"
	"strconv"
	"strings"
)

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

// split takes a string representation of a standard notation floating point number,
// and returns the integer and mantissa.
func split(s string) (*int64, *int64, error) {
	elements := strings.Split(s, ".")

	if len(elements) > 2 {
		return nil, nil, ErrParse{
			msg: errParseDefault,
			err: fmt.Errorf(
				"invalid string input %s",
				s,
			),
		}
	}

	var (
		source   *int64
		mantissa *int64
		err      error
	)

	if source, err = convert(elements[0]); err != nil {
		return nil, nil, err
	}

	if len(elements) == 2 {
		if mantissa, err = convert(elements[1]); err != nil {
			return nil, nil, err
		}
	}

	return source, mantissa, nil

}

// convert an alias to parse int, except handling the special case of empty strings as if they are "0"
// and returning an error meaningful to the package in error cases.
func convert(s string) (*int64, error) {
	var (
		converted int64
		err       error
	)
	if len(s) == 0 {
		converted = 0
		return &converted, nil
	}

	if converted, err = strconv.ParseInt(s, 10, 64); err != nil {
		// @TODO upgrade error types to incorporate internal errors as well for context.
		return nil, ErrParse{
			msg: errParseDefault,
			err: err,
		}
	}

	return &converted, nil
}

func scale(input int64) int {
	res := 0

	for input >= 1 {
		res = res + 1
		input = input / 10
	}

	return res
}

func scaleFromFrac(input float64) (int64, int, error) {
	// @TODO handle error case for input >= 1.0

	var (
		result   float64
		tmp      float64
		exponent int
	)

	result = input

	for {
		result, tmp = math.Modf(result)
		exponent = exponent + 1

		if exponent >= 18 {
			return 0, 0, ErrParse{
				msg: errParseDefault,
				err: nil,
			}
		}

		if !(tmp > 0) {
			break
		}
	}

	return int64(result), exponent, nil
}
