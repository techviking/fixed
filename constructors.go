package fixed

import (
	"fmt"
	"math"
	"strconv"
	"strings"
)

// Parse creates a new Fixed from a string, returning NaN, and error if the string could not be parsed.
func Parse(s string) *Fixed {
	if "NaN" == s {
		return NaN
	}

	if "" == s {
		return ZERO
	}

	if strings.ContainsAny(s, "eE") {
		return FromScientific(s)
	}

	var (
		main     *int64
		mantissa *int64
		exponent int
		err      error

		sign int64 = 1
	)

	if main, mantissa, err = split(s); err != nil {
		return FromNaN(&ErrNaN{
			reason: err.Error(),
		})
	}

	if *main < 0 {
		sign = -1
		*main = *main * -1
	}

	if mantissa == nil {
		exponent = 0
	} else {
		exponent = scale(*mantissa)
	}

	return &Fixed{
		main:     *main,
		mantissa: *mantissa,
		fp:       (*main)*int64(math.Pow10(exponent)) + (*mantissa),
		exponent: int64(exponent),
		sign:     sign,
	}
}

// MustParse creates a new Fixed from a string, and panics if the string could not be parsed
func MustParse(s string) *Fixed {
	f := Parse(s)
	if f.IsNaN() {
		panic(f.nan.reason)
	}
	return f
}

func FromScientific(s string) *Fixed {
	f, err := strconv.ParseFloat(s, 64)
	if err != nil {
		return FromNaN(&ErrNaN{
			reason: err.Error(),
		})
	}

	return FromFloat(f)
}

// FromFloat creates a Fixed from an float64
func FromFloat(f float64) *Fixed {
	if math.IsNaN(f) || math.IsInf(f, 0) {
		return FromNaN(&ErrNaN{
			reason: fmt.Sprintf("invalid constructor value %f", f),
		})
	}

	var (
		exponent int
		mantissa int64
		err      error
	)
	sign := int64(1)

	tmpMain, tmpMantissa := math.Modf(f)

	if tmpMain < 0 {
		sign = -1
	}

	main := int64(tmpMain)
	if mantissa, exponent, err = scaleFromFrac(tmpMantissa); err != nil {
		return FromNaN(&ErrNaN{
			reason: err.Error(),
		})
	}

	return &Fixed{
		main:     main,
		mantissa: mantissa,
		fp:       main*int64(math.Pow10(exponent)) + mantissa,
		exponent: int64(exponent),
		sign:     sign,
	}
}

// NewI creates a Fixed for an integer, moving the decimal point n places to the left
// For example, NewI(123,1) becomes 12.3.
func FromInt64(fp, exponent int64) *Fixed {

	str := strconv.FormatInt(fp, 10)
	index := len(str) - int(exponent)

	str = str[:index] + "." + str[index+1:]

	return Parse(str)
}

func FromNaN(err *ErrNaN) *Fixed {
	return &Fixed{
		nan: err,
	}
}

func (f Fixed) IsNaN() bool {
	return f.nan != nil
}

func (f Fixed) IsZero() bool {
	return f.fp == 0
}

