package fixed

import (
	"fmt"
	"math"
	"strconv"
)

// Float converts the Fixed to a float64
func (f Fixed) Float() float64 {
	if f.IsNaN() {
		return math.NaN()
	}

	if f.IsZero() {
		return 0
	}

	result, _ := strconv.ParseFloat(
		fmt.Sprintf("%d.%d", f.main, f.mantissa),
		64,
	)

	return result
}

// String converts a Fixed to a string, dropping trailing zeros
func (f Fixed) String() string {
	if f.IsNaN() {
		return "NaN"
	}

	if f.IsZero() {
		return "0"
	}

	return fmt.Sprintf("%d.%d", f.main, f.mantissa)
}

// StringN converts a Fixed to a String with a specified number of decimal places, truncating as required
func (f Fixed) StringN(decimals int) string {
	if f.IsNaN() {
		return "NaN"
	}

	mantissaStr := strconv.Itoa(int(f.mantissa))

	if len(mantissaStr) > decimals {
		mantissaStr = mantissaStr[:decimals]
	}

	return fmt.Sprintf(
		"%d.%s",
		f.main,
		mantissaStr,
	)
}

// Int return the integer portion of the Fixed, or 0 if NaN
func (f Fixed) Int() int64 {
	if f.IsNaN() {
		return 0
	}
	return f.main
}

// Frac return the fractional portion of the Fixed, or NaN if NaN
func (f Fixed) Frac() float64 {
	if f.IsNaN() {
		return math.NaN()
	}
	return float64(f.mantissa) / float64(f.exponent)
}
