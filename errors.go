package fixed

import (
	"fmt"
)

type (
	ErrTooLarge string
	ErrFormat string
	ErrParse struct {
		msg string
		err error
	}
	ErrNaN struct {
		reason string
	}
)

func (e ErrTooLarge) Error() string {
	return string(e)
}

func (e ErrFormat) Error() string {
	return string(e)
}

func (e ErrParse) Error() string {
	s, _ := e.MarshalJSON()
	return string(s)
}

func (e ErrParse) MarshalJSON() ([]byte, error) {
	if e.msg == "" {
		e.msg = errParseDefault
	}
	return []byte(
		fmt.Sprintf("{\"msg\":\"%s\", \"err\": \"%s\"}", e.msg, e.err),
	), nil
}

func (e ErrNaN) Error() string {
	return e.reason
}



const (
	errTooLarge = ErrTooLarge("significand too large")
	errFormat = ErrFormat("invalid encoding")
	errParseDefault = "cannot parse"
	errNaNDefault = "Default NaN"
)



